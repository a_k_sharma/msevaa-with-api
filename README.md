# Msevaa with API 

This projects works with CDAC Mobile Seva server to send SMS with valid template , this is integrated with Fast API to work flawlessly and efficiently. 

## Getting started

To get started with this project, we need to run the uvicorn command to run the web services. 

> _uvicorn : Uvicorn is a lightning-fast ASGI server implementation, using uvloop and httptools._

## Running this code 


First step is to run below commmand 
   
   
    uvicorn test:app --reload
    
**    This will show the following result **


    INFO:     Will watch for changes in these directories: ['/home/elf6/Desktop/Mobile Seva/MSeva API']
    INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
    INFO:     Started reloader process [6305] using watchgod
    INFO:     Started server process [6307]
    INFO:     Waiting for application startup.
    INFO:     Application startup complete.


This mean that our service is up and running , now go to /docs and enter message and mobile number._

