from typing import Optional

from fastapi import FastAPI
import finalcodemseva

app = FastAPI()

@app.get("/")
async def hello():
    return{"Hello":"World , Pleae refere to "/docs" in your URL to access Visuals"}


@app.get("/mseva/{mobile}/{message}")

async def read_item(mobile: str, message: str ):
    finalcodemseva.sent_name_message(mobile,message)
    return {
            "mobile": mobile,
             "message": message   
        }
