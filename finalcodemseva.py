#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 21:34:14 2021

@author: akrishna
"""
import http.client 
import requests
import pycurl 
import hashlib


username =  "Aiims"
password =  "Aiims@123"
senderid="AIIMSD" #senderid of the deparment
message=" AIIMS, New Delhi" #message content It must be in proper format as per template id
messageUnicode="DFFFDGFFHFHGFHFGFGFG" #message content in unicode
mobileno="8978184201" #if single sms need to be send use mobileno keyword
mobileNos= "86XXXXXX72,79XXXXXX00" #if bulk sms need to send use mobileNos as keyword and mobile number seperated by commas as value
deptSecureKey = "4dd830b5-9ea5-47ac-9bf1-9028ed99b77b"
encryp_password = hashlib.sha1(str(password).encode('utf-8'))
templateid= "1307161579789431013"
def encrypt_hash(message):
    hexcode = encryp_password.hexdigest() #converting the encoded data in hexadecimal
    #generating encoded key which consists of username, senderid , message and deptSecurekey and converting it in hexadecimal 
    key_plain = username.strip() + senderid.strip()+message.strip()+deptSecureKey.strip() 
    return (hashlib.sha512(key_plain.encode())).hexdigest(),hexcode

url = "https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT"
def post_to_url( url , data):
    field  = ''
    for var in data.keys() :
        field += var+"="+ data[var]+"&"
    field = field[:-1]      # removing the last "&"
   # print(field)            # checking what data is sent after all modifications
    post = pycurl.Curl()    #initiating Curl
    post.setopt(pycurl.SSL_VERIFYPEER , 0)
    post.setopt(pycurl.URL,url)
    post.setopt(pycurl.POST,len(data))
    post.setopt(pycurl.POSTFIELDS,field)
    #post.setopt(pycurl.TRANSFER_ENCODING,1)

    result = post.perform()
    print(result) #response from server 

    post.close() #closing curl
    
def sendSingleSMS(username,encryp_password,senderid
                  ,message,mobileno,deptSecureKey
                  ,templateid,key_enc,hexcode):
     data_dict = {
                "username" : username 
                , "password" : str(hexcode) 
                , "senderid" : senderid 
                , "content": message 
                , "smsservicetype" :"Singlemsg"  
                , "mobileno" : mobileno 
                , "key" :  key_enc
                , "templateid" : templateid
                }

     post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT", data_dict)

# #Below function sends sms to a single mobile number.
# sendSingleSMS(username,encryp_password,senderid,message,mobileno,deptSecureKey,templateid) 

def sent_name_message(item_id,message_id):
    full_msg = message_id+message
    key_enc, hexcode = encrypt_hash(full_msg)
    sendSingleSMS(username,encryp_password,senderid,full_msg,item_id,deptSecureKey,templateid,key_enc,hexcode) 


